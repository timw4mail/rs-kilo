# Oxidized Kilo

A reimplementation of the [Kilo](https://viewsourcecode.org/snaptoken/kilo/index.html)
tutorial in Rust. 

## Implementation notes:
* The `editor` prefix has been removed from all the editor methods. Since this implementation
uses `impl`s on a shared `Editor` struct, the prefix is redundant
* Any C equivalent functionality based on memory allocating/deallocating, or other manual book-keeping is instead
implemented in a more idiomatic Rust fashion.
* Row structs are referenced by their index in the Editor struct, rather than as a direct reference in method calls. 
This generally simplifies dealing with the rules of Rust (borrow checker).
* The `prompt` method of the editor can not take an arbitrary formatting string, due to Rust requiring a string literal 
for string formatting macros.

### Additions / Changes
* Reverse coloring for search results, so comment types can have different colors
* Two separate vectors are used to define the two types of keywords, rather than the weird pipe suffix